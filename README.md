# stm32f429i-discovery-lcd

This simple demo shows how to print a string on the 2.4" QVGA TFT LCD embedded on the STM32F429ZI Discovery kit.

STM32F429I-Discovery BSP Drivers / LCD Module is used.

Calling `BSP_LCD_Init` will initialize all the required peripheral, periph. clocks and pin-outs, so it not needed to set-up them manually in STM32CubeMX.

![alt](https://i.imgur.com/Bhc185L.png)

```
Project
├── Inc
│   ├── main.h
│   ├── stm32f4xx_hal_conf.h
│   └── stm32f4xx_it.h
├── project.elf.launch
├── project.ioc
├── Src
│   ├── main.c
│   ├── stm32f4xx_hal_msp.c
│   ├── stm32f4xx_it.c
│   └── system_stm32f4xx.c
├── startup
│   └── startup_stm32f429xx.s
└── STM32F429ZI_FLASH.ld
```

